# dash
from dash import Dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import dash_bootstrap_components as dbc

import flask


# for generating the session id, each user of the app receives a unique id
import uuid
import time

# user defined libraries
from app_utils import callbacks
from app_utils import controlpanel

###########
# LOGGING #
###########

import logging

#slower and saves on harddrive without a guaranteed way to remove it, no limitsize according to documentation, but there's a limit according to the browser


logger = logging.getLogger("app")
logger.setLevel(logging.DEBUG)

fh = logging.FileHandler('app.log')

fh.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s %(levelname)s - %(message)s')
fh.setFormatter(formatter)

logger.addHandler(fh)
logger.info("starting app...")


server = flask.Flask('app')

app = Dash( __name__,
            server=server,
            external_stylesheets=[dbc.themes.BOOTSTRAP])


app.title = "TRINITYconfigurator"

app.data = {} # stores session data for all users

# generates layout, should not be changed
def serve_layout(app): # little wrapper function
    
    def _serve_layout(): # actual layout function

        session_id = str(uuid.uuid4())
        now = time.time()

        app.data[session_id] = {}
        app.data[session_id]['__timestamp'] = now

        print(f'\n------------------------------------------\nnew session id requested: {session_id}')
        print(f'app.data at startup: {app.data}\n------------------------------------------\n')

        #clean old entries
        day = 24 * 60 * 60 # seconds 1 day
        
        for sid in app.data.keys():
            if now - app.data[sid]['__timestamp'] > 7 * day:
                print(f'deleting session_id {sid} as it is too old')
                del app.data[sid]

        session = html.Div(str(session_id), id='session-id', style={'display': 'none'}) # hidden state
        #table = elements.get_example_table()
        interface = controlpanel.layout()
        
        
        return html.Div([
            session,
            interface,
            #table,
        ])

    return _serve_layout #return layout function

app.layout = serve_layout(app) # defines layout of app

callbacks.register(app) # create callbacks
controlpanel.register(app)

if __name__ == "__main__":
    #app.run_server(debug=False, host='0.0.0.0', port=8109)
    app.run_server(debug=False, host='0.0.0.0', port=8120)
