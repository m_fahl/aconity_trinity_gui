# GUI LIBRARIES
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import dash_bootstrap_components as dbc

import re

# change import path so configuration_variables can be imported, check if linux compatible
import sys
import os
import pathlib

import flask
from flask.helpers import send_file


data_folder = pathlib.Path("app_utils")
sys.path[0] = data_folder.absolute().as_posix()
from controlpanel import list_of_topics



def dot_replacer(input_string):
    return re.sub("\.", "/", input_string)

list_of_topics_replaced = []
all_topic_ids = []
topic_more_than_once = []
for topic in list_of_topics:
    list_of_topics_replaced.append(dot_replacer(topic))

    if re.search("\.99\.", topic):
        topic_id = re.findall("[\d]+\.[\d]+\.[\d]+", topic)[0]
    else:
        topic_id = re.findall("[\d]+\.[\d]+", topic)[0]

    #keep track of all ids which exist more than once
    if topic_id in all_topic_ids and topic_id not in topic_more_than_once:
        topic_more_than_once.append(topic_id)

    all_topic_ids.append(topic_id)

#count the amount of base modules (Basismodule, modules starting with "01")
base_module_counter = 0
compulsory_module_counter = 0
for topic in list_of_topics:
    if topic.startswith("01"):
        base_module_counter += 1
    elif topic.startswith("02"):
        compulsory_module_counter += 1
    else:
        break



#header function, contains all callbacks and is imported by app.py
def register(app):

    server = app.server

    @app.callback(
        [
        Output("output_textfield", "children"),
        Output("placeholder", "children")
        ],
        [
        Input("submit", "n_clicks"),
        #Should be in State, but interfers with for loop
        Input("input_textfield_name", "value"),
        Input("input_textfield_number", "value"),
        ],
        [State(f"{module}", "value") for module in list_of_topics_replaced],
        prevent_initial_call = True
    )
    def input_calculator(button, project_name, project_number, *dropdowns):
        #if a dropdown has no value, it will return None


        trigger = dash.callback_context

        # ####### If the input text (project_name) was edited ####### #
        if trigger.triggered[0]["prop_id"] == "input_textfield_name.value" or trigger.triggered[0]["prop_id"] == "input_textfield_number.value":
            return dash.no_update

        base_modules_checked = False
        ids_checked = []

        #### Hashed out when Debugging

        optional_modules = ["02.99.09"]

        for cnt, dropdown in enumerate(dropdowns):

            #check if all base modules have been selected
            if cnt == base_module_counter:
                base_modules_checked = True
            elif base_modules_checked == False and dropdown == None:
                print(f"{list_of_topics[cnt]} is empty. Please choose a base module")
                return f"{list_of_topics[cnt]} is empty. Please choose a base module", "0"

            ##if module has to be selected but isn't, assuming you never have to select the same module twice (like 2 scanners or 2 lasers)
            #if dropdown == None and not re.search("03.", all_topic_ids[cnt]) and all_topic_ids[cnt] not in ids_checked and all_topic_ids[cnt] not in optional_modules:
            if dropdown == None and re.search("01.", all_topic_ids[cnt]) and all_topic_ids[cnt] not in ids_checked:
                print(f"{list_of_topics[cnt]} is empty. This module has to be chosen")
                return f"{list_of_topics[cnt]} is empty. This module has to be chosen", "0"

            if dropdown != None:
                if re.search("\.99\.", dropdown):
                    ids_checked.append(re.findall("[\d]+\.[\d]+\.[\d]+", dropdown)[0])
                else:
                    ids_checked.append(re.findall("[\d]+\.[\d]+", dropdown)[0])

        print(f"project_name is {project_name}")
        if project_name == None or project_name == "":
            print("project_name is empty. Please choose a name for your project.")
            return "project_name is empty. Please choose a name for your project.", "0"

        if project_number == None or project_number == "":
            print("project_number is empty. Please choose a number for your project.")
            return "project_number is empty. Please choose a number for your project.", "0"

        ####



        if len(all_topic_ids) != len(dropdowns):
            print(f"Recieved 2 arrays of different lengths. all_topic_ids is {all_topic_ids} and dropdowns is {dropdowns}, {len(all_topic_ids), len(dropdowns)}")

        #prepare Output
        modules = []
        #keep track of the topics already in use
        modules_str = ""

        for cnt, topic in enumerate(all_topic_ids):

            if dropdowns[cnt] == None:
                variant = "000"
            else:
                variant = re.findall("_[\d]+", dropdowns[cnt])[0]
                variant = re.findall("[\d]+", variant)[0]

            #returns 0 if topic hasn't been used and counts up for every previous use. Laser 1 = 0, Laser 2 = 1
            index = len(re.findall(f"{topic}, ", modules_str))

            #if topic exists more than once index has to start at 1
            if topic in topic_more_than_once:
                index += 1

            modules.append({"id" : topic, "variant" : variant, "index" : index})
            modules_str += f"{topic}, "

        output_string = str({
            "name": project_name,
            "project": project_number,
            "modules": modules
        })
        

        #delete json file previously saved at this spot
        try:
            delete_old_file = pathlib.Path("download/Configurator_test.json")
            delete_old_file.unlink(missing_ok = True)
        except:
            os.remove("download/Configurator_test.json")
        
        with open(pathlib.Path("download").as_posix() + "/Configurator_test.json", "a") as txt:
            for char in output_string:
                if char == "'":
                    char = '"'
                txt.write(char)
        
        return output_string, pathlib.Path("download").as_posix() + "/Configurator_test.json"



    @server.route("/download/<path>")
    def download_file (path = None):
        return send_file("download/" + path, as_attachment=True)




####### Modal Window #######

    def build_download_button(uri):
        print(f"uri is {uri}")
        """Generates a download button for the resource"""
        # button = html.Form(
        #     action=uri,
        #     method="get",
        #     children=[
        #         html.Button(
        #             id = "download_button",
        #             className="button",
        #             type="submit",
        #             children=[
        #                 "Download"
        #             ]
        #         )
        #     ]
        # )
        button = html.A(
            href=uri,
            download ="Configurator_test.json",
            children=[
                html.Button(
                    id = "download_button",
                    className="button",
                    type="submit",
                    children=[
                        "Download"
                    ]
                )
            ]
        )
        return button
      


    @app.callback(
        [
        Output("modal_body", "children"),
        Output("modal", "is_open")
        ],
        Input("placeholder", "children"),
        [State("modal", "is_open")],
        prevent_initial_call = True
    )
    def toggle_modal(path, is_open):

        if path == "0":
            raise PreventUpdate
        else:
            download_button = html.Div([build_download_button(path)])
        
        trigger = dash.callback_context


        if trigger.triggered[0]["prop_id"] == "placeholder.children":
            return download_button, True
        else:
            return download_button, False


# {
#     "name": "Test",
#     "project": 1234,
    # "modules": [
    #     {
    #         "id": "01.01",
    #         "variant": "001",
    #         "index": 1
    #     }
    # ]
# }