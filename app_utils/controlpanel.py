import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import dash_bootstrap_components as dbc

import re

# from os import path
# data_folder = path.dirname(path.dirname(__file__)) + "\\assets"
# file_to_open = data_folder + "\\module_list.txt"

import pathlib

data_folder = pathlib.Path("assets")
file_to_open = data_folder.absolute().as_posix() + "/module_list.txt"


aconity_logo = html.Img(
    id = "Aconity_Icon",
    src = data_folder.as_posix() + "/logo-footer-aconity3d.png",
)


def dot_replacer(input_string):
    return re.sub("\.", "/", input_string)



with open(file_to_open, "r", encoding='utf8') as txt:
    data_folder.absolute().as_posix()
    textfile = txt.read()
    #textfile_split[0] is a string of all topics, textfile_split[1] is a string of all modules
    textfile_split = re.split("[\W]*\+\+\+\+\+ Optionen \+\+\+\+\+[\W]*", textfile)

dict_of_modules = {}
'''
dict_of_modules[t] = dict containing a list
dict_of_modules[t][0] = list of topic and topic with . replaced by / so dash can use them for id
dict_of_modules[t][1] = modules that can be chosen for this topic
dict_of_modules[t][2] = modules that can be chosen for this topic with . replaced by / so dash can use them for id
dict_of_modules[t][3] = set to False per default, gets replaced by a list if topic id is assigned multiple times
'''
for line in textfile_split[0].split("\n"):

    # use topic id as dictionary key
    if re.search("\.99\.", line):
        dict_title = re.findall("[\d]+\.[\d]+\.[\d]+", line)[0]
    else:
        dict_title = re.findall("[\d]+\.[\d]+", line)[0]


    ##check if even neccessary##################################################
    #check if topic name is already in use
    try:
        assert(dict_of_modules[dict_title][0] != False)
        
        #unset default value
        if dict_of_modules[dict_title][0][3] == False:
            dict_of_modules[dict_title][0][3] = []
        #save additional topic in same dict
        dict_of_modules[dict_title][0][3].append([line, dot_replacer(line)])
    
    #topic name has to be created
    except:
        dict_of_modules[dict_title] = ["", [], [], False]
        dict_of_modules[dict_title][0] = [line, dot_replacer(line)]

list_of_topics = textfile_split[0].split("\n")


# assign modules to topics
for line in textfile_split[1].split("\n"):

    # remove whitespace, article number and ''
    line = re.sub("[\w\W]*', '", "", line)
    line = line.strip("'")

    # append modules to topic
    if re.search("\.99\.", line):
        dict_of_modules[re.findall("[\d]+\.[\d]+\.[\d]+", line)[0]][1].append(line)
        dict_of_modules[re.findall("[\d]+\.[\d]+\.[\d]+", line)[0]][2].append(dot_replacer(line))
    else:
        dict_of_modules[re.findall("[\d]+\.[\d]+", line)[0]][1].append(line)
        dict_of_modules[re.findall("[\d]+\.[\d]+", line)[0]][2].append(dot_replacer(line))




list_of_dropdowns = [[], [], []]


for topic in list_of_topics:
    '''
    Put all variables beginning with _01 in list_of_dropdowns[0], _02 in list_of_dropdowns[1] etc.
    Assign them a textfield with their name and a dropdown-menu with the appropriate number of options
    '''
    #read if module belongs to class 1,2 or 3
    dropdown_container = int(topic[1]) - 1
    #make id readable for dash
    topic_replaced = dot_replacer(topic)


    if re.search("\.99\.", topic):
        dropdown_options = dict_of_modules[re.findall("[\d]+\.[\d]+\.[\d]+", topic)[0]][1]
    else:
        dropdown_options = dict_of_modules[re.findall("[\d]+\.[\d]+", topic)[0]][1]



    list_of_dropdowns[dropdown_container].append(
        html.Div(
            children = [
                html.Table([
                    html.Tr(
                        children = [
                            html.Td(
                                html.P(
                                    contentEditable = False,
                                    children = topic,
                                    style = {"width" : "15em", "overflow-wrap" : "break-word", "margin-top": "1.5em", "margin-left" : "10px"},
                                )
                            ),
                            html.Td(
                                dcc.Dropdown(
                                    id = topic_replaced,
                                    options = [{'label': option, 'value': option} for option in dropdown_options],
                                    style = {"min-width" : "18em"},
                                    placeholder = "000",
                                )
                            )
                        ]
                    )
                ])
            ]
        )
    )

table_of_tables_header = html.Div(
    children = [
        html.Table([
            html.Tr([
                html.Td(
                    children = html.P(
                        id = "base_module_header",
                        children = "Base Modules",
                        style = {"padding-left" : "3em"}
                        #style = {"font-size" : "x-large", "width" : "33em", "margin-left" : "10px"},
                    )
                ),
                html.Td(
                    children = html.P(
                        id = "compulsory_module_header",
                        children = "Compulsory Modules",
                        #style = {"font-size" : "x-large", "width" : "33em", "margin-left" : "10px"},
                    )
                ),
                html.Td(
                    children = html.P(
                        id = "optional_module_header",
                        children = "Optional Modules",
                        #style = {"font-size" : "x-large", "width" : "33em", "margin-left" : "10px"},
                    )
                ),
            ])
        ])
    ]
)

table_of_tables = html.Div(
    children = [
        html.Table(
            id = "table_of_tables_parent",
            children =[
                html.Tr(
                    #style = {"minWidth" : "30em"},
                    children = [
                        html.Td(
                            id = "table_of_tables_1",
                            children = list_of_dropdowns[0]
                        ),
                        html.Td(
                            id = "table_of_tables_2",
                            children = list_of_dropdowns[1]
                        ),
                        html.Td(
                            id = "table_of_tables_3",
                            children = list_of_dropdowns[2]
                        )
                    ]
                )
            ])
    ]
)

output_textfield_P = html.P(
    id = "output_textfield",
    contentEditable = False,
    children = "Please choose all base and compulsory modules and set a project name"
)


input_textfield_T = html.Div(
    id = "input_textfield_div",
    children = [
        html.Table([
            html.Tr([
                html.Td([
                    html.P(   
                        contentEditable = False,
                        children = "Project Name:",
                        style = {"width" : "10em", "color" : "inherit", "font-size" : "inherit", "text-align" : "right", "margin-right" : "15px", "margin-top" : ".8em"},

                    ),
                    html.P(   
                        contentEditable = False,
                        children = "Project Number:",
                        style = {"width" : "10em", "color" : "inherit", "font-size" : "inherit", "text-align" : "right", "margin-right" : "15px", "margin-top" : "1.6em"},

                    ),
                ]),
                html.Td([
                    dcc.Textarea(
                    id = "input_textfield_name",
                    rows = "1",
                    #placeholder = "Enter your project name",
                    style = {"width" : "30em", "height" : "2em", "color" : "inherit", "background-color" : "black", "font-size" : "inherit", "margin-top" : ".4em"}
                    ),
                    dcc.Textarea(
                    id = "input_textfield_number",
                    rows = "1",
                    #placeholder = "Enter your project number",
                    style = {"width" : "30em", "height" : "2em", "color" : "inherit", "background-color" : "black", "font-size" : "inherit", "margin-top" : ".8em"}
                    ),
                ])
            ]
            )
        ])
    ]
)

filler_object = html.P(
    id = "placeholder",
    style = {"display": "none"},
    children = ""
)


modal = html.Div(
    [
        dbc.Modal(
            [
                dbc.ModalHeader("Do you want to download the JSON file?"),
                dbc.ModalBody(id = "modal_body"),
                dbc.ModalFooter(),
            ],
            id="modal",
            size="lg",
            backdrop = True,
            #style = {"position" : "absolute", "top" : "-100vh"}
        ),
    ]
)

def layout():

    return html.Div(
        id = "wrapper",
        children = [
            input_textfield_T,
            output_textfield_P,
            table_of_tables_header,
            html.Div(
                id = "dont-put-anything-else-in-here",
                children = table_of_tables
            ),
            html.Button(
                id = "submit",
                children = "Submit All",
            ),
            aconity_logo,
            filler_object,
            modal,
        ]
    )

def register(app):
    pass
  
    # @app.callback()

    # def ():