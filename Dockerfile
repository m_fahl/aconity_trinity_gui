FROM python:3.7-slim

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/requirements.txt

RUN python -m pip install -r requirements.txt

COPY . /usr/src/app

CMD ["python", "app.py"]

EXPOSE 8301