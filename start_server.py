from gevent.pywsgi import WSGIServer
from app import app
import sys

import os


ip = '0.0.0.0'
# port = 5000

try:
    port = int(os.environ['PORT'])
    http_server = WSGIServer((ip, port), app.server)
    http_server.serve_forever()
except ValueError as e:
    print('exception trying to read out port env variable: %s' % e)
    print('exiting....')

